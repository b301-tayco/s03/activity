import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed");

        Scanner input = new Scanner(System.in);

        int num = 0;

        try {
            num = input.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid. Input should be an integer");
            e.printStackTrace();
        }
         int answer = 1;
         int counter = 1;


         //WHILE LOOP

        while (counter <= num){
            answer *= counter;
            counter++;
        }
 /*

        //FOR LOOP

        for (int i = 1; i <= num; i++){
            answer *= i;
        }

*/


        if(num == 0){
            System.out.println("Factorial of " + num + " is " + answer);
        } else if (num<0) {
            System.out.println("Cannot compute for factorials of negative numbers.");
        } else {
            System.out.println("Factorial of " + num + " is " + answer);
        }




    }
}